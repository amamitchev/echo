package org.echo.controller;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

@RestController
public class EchoController {
	
	private Logger log = LoggerFactory.getLogger(EchoController.class);

	@GetMapping(value = "/echo/{src}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public Flux<Object> echo(@PathVariable("src") String src) {
		return Flux.generate(sink -> {
			sink.next(src);
			log.info("next: " + src);
		})
		.delayElements(Duration.ofSeconds(1), Schedulers.single())
		.doOnTerminate(() -> log.info("Terminated!!!"));
	}

}
