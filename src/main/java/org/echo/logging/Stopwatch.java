package org.echo.logging;

import org.springframework.stereotype.Component;

/**
 * Часы для измерения длины интервала.
 */
@Component
public class Stopwatch {

    public Timing start() {
        Timing timing = new Timing();
        timing.timeMs = System.currentTimeMillis();
        timing.timeNs = System.nanoTime();
        return timing;
    }

    public Timing finish(Timing timing) {
        timing.timeMs = System.currentTimeMillis() - timing.timeMs;
        timing.timeNs = System.nanoTime() - timing.timeNs;
        return timing;
    }

    /**
     * Timing holder object.
     */
    public static class Timing {
        long timeMs;
        long timeNs;

        public long getTimeMs() {
            return timeMs;
        }

        public long getTimeNs() {
            return timeNs;
        }
    }

}
