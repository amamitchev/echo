package org.echo.model;

public abstract class AbstractBuilder<B extends AbstractBuilder<B, R>, R> {
    protected abstract B self();
    public abstract R build();
}
