package org.echo.model;

public class Model2D extends Model1D {

    private final int width;

    public int getWidth() {
        return width;
    }

    public Model2D(BuilderBase<?, ?> builder) {
        super(builder);
        this.width = builder.width;
    }

    static abstract class BuilderBase<B extends BuilderBase<B, R>, R> extends Model1D.BuilderBase<B, R> {

        int width;

        public B withWidth(int v) {
            width = v;
            return self();
        }

    }

    static class Builder extends BuilderBase<Builder, Model2D> {

        @Override
        public Model2D build() {
            return new Model2D(this);
        }

        @Override
        protected Builder self() {
            return this;
        }

    }

}
