package org.echo.model;

public class Model1D {

    private final Color color;
    private final int length;

    public Color getColor() {
        return color;
    }

    public int getLength() {
        return length;
    }

    public Model1D(BuilderBase<?, ?> builder) {
        // super(builder);
        this.color = builder.color;
        this.length = builder.length;
    }

    static abstract class BuilderBase<B extends BuilderBase<B, R>, R> extends AbstractBuilder<B, R> {

        Color color;
        int length;

        public B withColor(Color v) {
            color = v;
            return self();
        }

        public B withLength(int v) {
            length = v;
            return self();
        }

    }

    static class Builder extends BuilderBase<Builder, Model1D> {

        @Override
        public Model1D build() {
            return new Model1D(this);
        }

        @Override
        protected Builder self() {
            return this;
        }

    }

}
