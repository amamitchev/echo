package org.echo.model;

public class Model3D extends Model2D {

    private final int height;

    public int getHeight() {
        return height;
    }

    public Model3D(BuilderBase<?, ?> builder) {
        super(builder);
        this.height = builder.height;
    }

    static abstract class BuilderBase<B extends BuilderBase<B, R>, R> extends Model2D.BuilderBase<B,R> {

        int height;

        public B withHeight(int v) {
            height = v;
            return self();
        }

    }

    static class Builder extends BuilderBase<Builder, Model3D> {

        @Override
        public Model3D build() {
            return new Model3D(this);
        }

        @Override
        protected Builder self() {
            return this;
        }

    }

}
