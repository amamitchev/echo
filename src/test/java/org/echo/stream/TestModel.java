package org.echo.stream;

/**
 * Тестовая модель.
 */
public final class TestModel {
    private final String region;
    private final String district;
    private final String name;

    public String getRegion() {
        return region;
    }

    public String getDistrict() {
        return district;
    }

    public String getName() {
        return name;
    }

    public TestModel(String region, String district, String name) {
        this.region = region;
        this.district = district;
        this.name = name;
    }

}
