package org.echo.model;

import org.junit.jupiter.api.Test;

import static org.echo.model.Color.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Model3DTest {

    @Test
    void test1D() {
        Model1D model = new Model1D.Builder()
                .withColor(RED)
                .withLength(100)
                .build();
        assertEquals(RED, model.getColor());
    }

    @Test
    void test2D() {
        Model2D model = new Model2D.Builder()
                .withColor(GREEN)
                .withLength(100)
                .withWidth(10)
                .build();
        assertEquals(GREEN, model.getColor());
    }

    @Test
    void test3D() {
        Model3D model = new Model3D.Builder()
                .withColor(BLUE)
                .withLength(100)
                .withWidth(10)
                .withHeight(20)
                .build();
        assertEquals(BLUE, model.getColor());
    }

}
