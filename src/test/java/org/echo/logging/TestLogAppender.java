package org.echo.logging;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

/**
 * Log appender для тестирования вызовов логгинга.
 */
public class TestLogAppender extends AppenderBase<ILoggingEvent> {

    private final List<ILoggingEvent> list = new ArrayList<>();
    private final List<String> msgList = new ArrayList<>();
    private final PatternLayoutEncoder encoder;

    public TestLogAppender() {
        this((LoggerContext) LoggerFactory.getILoggerFactory());
    }

    public TestLogAppender(LoggerContext loggerContext) {
        super.setContext(loggerContext);
        this.encoder = new PatternLayoutEncoder();
        this.encoder.setContext(loggerContext);
        encoder.setPattern("%msg");
        encoder.start();
        Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logger.setLevel(Level.INFO);
        logger.addAppender(this);
    }

    @Override
    protected void append(ILoggingEvent event) {
        list.add(event);
        msgList.add(new String(encoder.encode(event)));
    }

    public List<ILoggingEvent> events() {
        return list;
    }

    public List<String> messages() {
        return msgList;
    }

    public void cleanup() {
        list.clear();
        msgList.clear();
    }

}
