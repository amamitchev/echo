package org.echo.logging;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * Test aspect for {@link TestLogging} annotation.
 */
@Aspect
@Component
public class TestLoggingAspect extends LoggingAspect {

    @Override
    @Pointcut("@within(org.echo.logging.TestLogging) || "
            + "@annotation(org.echo.logging.TestLogging)")
    public void methodCall() {
    }

}
