package org.echo.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test service for {@link LoggingAspect}.
 */
public class LoggingAspectTestService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspectTestService.class);

    @TestLoggingOverride(values = {@LogValue(name = "val1", expr = "'boo1'"),
            @LogValue(name = "val2", expr = "'boo2'")})
    public void exceptionMethod() {
        LOGGER.info("Exception method");
        throw new RuntimeException("Exception message");
    }

    @TestLogging("LOGGER")
    public void noArgsMethod() {
        LOGGER.info("No arguments method");
    }

    @TestLogging
    public void wrongLoggerMethod() {
        LOGGER.info("No arguments method");
    }

    @TestLogging("LOGGER")
    public String basicArgsMethod(String param1) {
        LOGGER.info("Simple arguments method");
        return "basicArgsMethodResult";
    }

}
