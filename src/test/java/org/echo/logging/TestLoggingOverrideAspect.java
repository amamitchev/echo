package org.echo.logging;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * Test aspect for {@link TestLoggingOverride} annotation.
 */
@Aspect
@Component
public class TestLoggingOverrideAspect extends LoggingAspect {

    @Override
    @Pointcut("@within(org.echo.logging.TestLoggingOverride) || "
            + "@annotation(org.echo.logging.TestLoggingOverride)")
    public void methodCall() {
    }

}
