package org.echo.logging;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target({TYPE, METHOD})
@Logging(value = "LOGGER",
        startMsg = "ENTER: start message, val1 = {val1}, val2 = {val2}",
        endMsg = "EXIT: start message, val1 = {val1}, val2 = {val2}, time {timeMs}ms",
        errorMsg = "ERROR: error message, class = {errClass}, message = {errMsg}, time {timeMs}ms",
        values = {})
public @interface TestLoggingOverride {
    LogValue[] values() default {};
}
